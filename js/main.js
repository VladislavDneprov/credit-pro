jQuery(function($){

	function Menu(){
		var top = $(document).scrollTop();
		if (top < 800) {
			$("#toTop").removeClass('float');							
		}				
		else {
			$("#toTop").addClass('float');						
		}
	}

	$(document).ready(function(){

		$('#toTop').click(function(){
	        var el = $(this).attr('href');
	        $('body').animate({
	            scrollTop: $(el).offset().top}, 700);
	        return false; 
		});
		

		$('.top-slider').slick({
			slidesToShow: 1,
		    slidesToScroll: 1,
		    autoplay: true,
		    autoplaySpeed: 1500,
		    dots: false,
		    infinite: true,
		    arrows: true,
		   	nextArrow: '<div class="slider-arrows next"></div>',
		   	prevArrow: '<div class="slider-arrows prev"></div>',
		    responsive: [
			    {
			      breakpoint: 1199,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			      }
			    },		    
	    	]
		});
		
	});
});