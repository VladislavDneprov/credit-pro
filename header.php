<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Money online</title>

        <?php wp_head(); ?>
    </head>
    
    <body>
	    <header id="top">
            <div class="wide-container"> 
                <div class="row">         
                    <a href="<?= get_home_url(); ?>" class="logo col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center-sm">
                        <img src="<?= get_template_directory_uri().'/img/logo.png';?>">
                    </a>
        	    	<!--<nav class="menu col-lg-8 col-md-8 col-sm-6 col-xs-12">
                        <ul>
                            <li>
                                <a href="#">Главная</a>
                            </li>
                            <li>
                                <a href="#">Оптимальный кредит</a>
                            </li>
                            <li>
                                <a href="#">Просчёт кредита</a>
                            </li>
                            <li>
                                <a href="#">Новости</a>
                            </li>
                        </ul>      
                    </nav>-->
                    <?= wp_nav_menu(array('theme_location' => 'top', 'container_class' => 'menu col-lg-8 col-md-8 col-sm-8 col-xs-12','container' => 'nav')); ?>
                </div>  
            </div>
	    </header>
        <section class="top-slider">
            <?php
            $args = array(
                'numberposts'     => -1,
                'offset'          => 0,
                'orderby'         => 'post_date',
                'order'           => 'DESC',
                'include'         => '',
                'exclude'         => '',
                'post_type'       => 'slider',
                'post_parent'     => '',
                'post_status'     => 'publish'
            );

            $slider = get_posts($args);
            foreach ($slider as $key => $value):?>
                <div class="item">
                    <?=get_the_post_thumbnail($value->ID);?>
                    <div class="desc">
                        <div class="wide-container">
                            <div class="content">
                                <p><?=$value->post_content;?> </p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </section>