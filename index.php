<?php 
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage credit-pro
 */
get_header(); ?>

<section class="features">
	<div class="wide-container row">			
		<div class="feature-block col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<img src="<?= get_template_directory_uri().'/img/clock.png' ?>">
			<div class="desc">
				<span class="title">Скорость оформления</span>
				<span>Принятие заявки от 5 минут</span>
			</div>
		</div>
		<div class="feature-block col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<img src="<?= get_template_directory_uri().'/img/credit.png' ?>">
			<div class="desc">
				<span class="title">Кредит до 300.000 грн</span>
				<span>Максимум преймуществ от 
					предоставленных банков Украины</span>
			</div>
		</div>
		<div class="feature-block col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<img src="<?= get_template_directory_uri().'/img/clipboard.png' ?>">
			<div class="desc">
				<span class="title">Ваше предпочтение</span>
				<span>Выбирайте подходящие для Вас
					условия кредитования</span>
			</div>
		</div>
	</div>
</section>
<section class="">
	<div class="wide-container">
		<div class="filter">
			<div class="btn-group">
	            <form action="<?= get_home_url(); ?>" method="post">
	                <button type="submit" class="btn">Все</button>
	            </form>
	            <?php
	            $pages = get_pages(array(
	                'numberposts' 		=> -1,
	                'post_status' 		=> 'publish',
	                'parent'			=> get_page_by_path('country')->ID,
	                'sort_column'		=> 'menu_order'
	            ));
	            ?>
	            <?php foreach ($pages as $key => $page): ?>
	                <form action="<?= get_permalink($page->ID); ?>" method="post">
	                    <input type="hidden" value="<?= $key+1 ?>" name="value" />
	                    <input type="hidden" value="country" name="key" />
	                    <button type="submit" class="btn"><?= $page->post_title; ?></button>
	                </form>
	            <?php endforeach; ?>
	        </div>
		</div>
		<div class="head-table">
			<ul>
				<li>Кредитор</li>
				<li>Время: <span>за 15 минут</span></li>
				<li>Сумма: <span>до 10000</span></li>
				<li>Срок: <span>до 60 дней</span></li>
				<li>Возраст: <span>с 18 лет</span></li>
			</ul>
		</div>
		<?php get_template_part('part/list-companies'); ?>
	</div>
</section>
<section class="credit">
	<div class="wide-container">
		<div class="credit-header">
			Получение кредита в 3 шага
		</div>
		<div class="credit-body row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="credit-block">
					<div class="before">1 шаг</div>
					<div class="desc">
						<p>Выбор подходящего для вас кредитного плана</p>
					</div>
					<div class="img-container">
						<img src="<?= get_template_directory_uri().'/img/credit-clock.png' ?>">
					</div>			
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="credit-block">
					<div class="before">2 шаг</div>
					<div class="desc">
						<p>Заполнение заявки на сайте финансовой компании</p>
					</div>
					<div class="img-container">
						<img src="<?= get_template_directory_uri().'/img/credit-rows.png' ?>">
					</div>			
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="credit-block">
					<div class="before">3 шаг</div>
					<div class="desc">
						<p>Получение денег после рассмотрения заявки</p>
					</div>
					<div class="img-container">
						<img src="<?= get_template_directory_uri().'/img/credit-book.png' ?>">
					</div>			
				</div>
			</div>
			<?php get_template_part('part/content-page'); ?>
		</div>
	</div>
</section>


<?php get_footer(); ?>