<?php
/**
 * Template Name: Page
 * @package WordPress
 * @subpackage credit-pro
 */
get_header(); ?>

	<section class="features">
		<div class="wide-container row">
				<?php 
				if ( have_posts() && is_page() ) :
					while ( have_posts() ) : the_post(); ?>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2><?php the_title(); ?></h2></div>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php get_template_part('part/content-page'); ?>
			</div>
		</div>
	</section>


<?php get_footer(); ?>