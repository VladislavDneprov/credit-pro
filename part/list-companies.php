<?php

$meta_key = $_POST['key'];
$meta_value = $_POST['value'];
$sort = ($_POST['sort'] ? 'meta_value_num' : 'ID');

$args = array(
	'numberposts'     => -1,
	'offset'          => 0,
	'orderby'         => $sort,
	'order'           => 'DESC',
	'meta_key'        => ($meta_key ? $meta_key : ''),
	'meta_value'      => ($meta_value ? $meta_value : ''),
	'post_type'       => 'company',
	'post_parent'     => '',
	'post_status'     => 'publish'
);

$companies = get_posts($args);

$companies_best = get_posts(array(
    'numberposts'     => -1,
    'offset'          => 0,
    'orderby'         => $sort,
    'order'           => 'DESC',
    'meta_key'        => 'top',
    'meta_value'      => 1,
    'post_type'       => 'company',
    'post_parent'     => '',
    'post_status'     => 'publish'
));

$countries = get_pages(array(
				'numberposts' 		=> -1,
				'post_status' 		=> 'publish',
				'parent'			=> get_page_by_path('country')->ID,
				'sort_column'		=> 'menu_order'
));

foreach ($countries as $key => $page)
	$array_countries[$key+1] = $page->post_title;

$current_id = get_queried_object_id();
$page = get_post($current_id);
?>
<div class="main-table">
	<?php foreach ($companies_best as $key => $post): ?>
        <?php if ($page->post_title != 'РФ'): ?>
		<ul <?=(get_post_meta($post->ID, 'top', 1) ? 'class="best"' : ""); ?>>
			<li class="img"><?= get_the_post_thumbnail($post->ID); ?></li>
			<li><?= get_post_meta($post->ID, 'time', true); ?> мин</li>
			<li class="sum"><?= get_post_meta($post->ID, 'summ', true); ?>грн<a href="<?= get_post_meta($post->ID, 'url', true); ?>" title="Подать заявку" class="btn btn-primary">Подать заявку</a></li>
			<li><?= get_post_meta($post->ID, 'time_get', true); ?></li>
			<li class="country-label"><?= crop_string($array_countries[get_post_meta($post->ID, 'country', true)], 6); ?></li>
			<li><?= get_post_meta($post->ID, 'age', true); ?></li>
		</ul>
            <?php endif; ?>
	<?php endforeach; ?>
	<?php foreach ($companies as $key => $post){ ?>
    <?php if ($page->post_title == 'РФ' && $key == 2){
            break;
        } ?>
    <?php if ($page->post_title != 'Украина'): ?>
		<?php if (!get_post_meta($post->ID, 'top', 1)): ?>
			<ul <?=(get_post_meta($post->ID, 'top', 1) ? 'class="best"' : ""); ?>>
				<li class="img"><?= get_the_post_thumbnail($post->ID); ?></li>
				<li><?= get_post_meta($post->ID, 'time', true); ?> мин</li>
				<li class="sum"><?= get_post_meta($post->ID, 'summ', true); ?>грн<a href="<?= get_post_meta($post->ID, 'url', true); ?>" title="Подать заявку" class="btn btn-primary">Подать заявку</a></li>
				<li><?= get_post_meta($post->ID, 'time_get', true); ?></li>
				<li class="country-label"><?= crop_string($array_countries[get_post_meta($post->ID, 'country', true)], 6); ?></li>
				<li><?= get_post_meta($post->ID, 'age', true); ?></li>
			</ul>
		<?php endif; ?>
        <?php endif; ?>
	<?php } ?>
</div>