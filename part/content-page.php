<?php
	$current_id = get_queried_object_id();
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php 
		if ( have_posts() && is_page() ) :
			while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
			<?php endwhile; ?>
		<?php else: ?>
			<?= get_page_by_path('main')->post_content ?>
		<?php endif; ?>
</div>