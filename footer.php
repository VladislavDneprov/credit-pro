<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage money-online
 */
 ?>
		 <footer>
		 	<div class="wide-container">
		 		<a href="https://credit365.ua/rug" class="btn">Больше информации</a>
		 		<img src="<?= get_template_directory_uri().'/img/logo-footer.png';?>">
		 		<?= wp_nav_menu(array('theme_location' => 'footer', 'container_class' => '','container' => 'ul')); ?>
		 	</div>
		 </footer>
		 <?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
	</body>
</html>